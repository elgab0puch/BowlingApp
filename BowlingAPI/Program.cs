using Bowling.Core.Application.Implementations.DependencyInjection;
using Bowling.Infrastructure.Persistence.DependencyInjection;
using Bowling.Infrastructure.Persistence.Implementations;
using Bowling.Infrastructure.Presentation.RestApi.Mappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

builder.Services.AddDbContext<PostgreDbContext>(
    options => options.UseNpgsql(builder.Configuration.GetConnectionString("BowlingPostgreDB"))
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));

//builder.Services.Configure<MongoDbSettings>(
//builder.Configuration.GetSection("MongoDBSettings"));

//builder.Services.AddSingleton<MongoDbContext>();
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Bowling", Version = "v1" });
});

builder.Services
    .AddApplicationServices();
builder.Services
    .AddDatabaseRepositories();
builder.Services.AddAutoMapper(typeof(GameProfile));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BowlingApi v1"));
}

app.UseRouting();
app.UseAuthorization();
app.MapControllers();
app.UseEndpoints(endpoints => endpoints.MapControllers());
app.Run();
