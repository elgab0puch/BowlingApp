﻿namespace Bowling.Core.Domain.Model
{
    public class Frame
    {
        public Guid Id { get; set; }
        public int TotalKnockedPins { get; set; }
        public bool IsSplit { get; set; }  
        public bool IsStrike { get; set; } 
        public Game Game { get; set; }
    }
}
