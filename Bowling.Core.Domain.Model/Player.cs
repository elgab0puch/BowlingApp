﻿namespace Bowling.Core.Domain.Model
{
    public class Player
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<Game> Games { get; set; }
    }
}
