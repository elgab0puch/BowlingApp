﻿namespace Bowling.Core.Domain.Model
{
    public class Game
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public int Score { get; set; }
        public Player Player { get; set; }
        public List<Frame> Frames { get; set; }
    }
}
