﻿namespace Bowling.Core.Application.Implementations.DependencyInjection
{
    using Bowling.Core.Application.Abstractions.Services;
    using Bowling.Core.Application.Implementations.Services;
    using Microsoft.Extensions.DependencyInjection;
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IPlayerService, PlayerService>();
            services.AddScoped<IGameService, GameService>();
            services.AddScoped<IFrameService, FrameService>();

            return services;
        }
    }
}
