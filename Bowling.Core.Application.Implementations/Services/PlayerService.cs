﻿namespace Bowling.Core.Application.Implementations.Services
{
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Application.Abstractions.Services;
    using Bowling.Core.Domain.Model;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    public class PlayerService : IPlayerService
    {
        private readonly IPlayerRepository _playerRepository;

        public PlayerService(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task<Player> CreatePlayerAsync(Player player)
        {
            Player existingPlayer = await _playerRepository.GetPlayerByEmailAsync(player.Email);
            if (existingPlayer != null)
            {
                return existingPlayer;
            }

            return await _playerRepository.CreatePlayerAsync(player);
        }

        public async Task<IEnumerable<Player>> GetAllPlayersAsync()
        {
            return await _playerRepository.GetAllPlayersAsync();
        }

        public async Task<Player> GetPlayerByIdAsync(Guid id)
        {
            Player player = await _playerRepository.GetPlayerByIdAsync(id);

            if (player == null)
                throw new ArgumentException($"Player with id {id} not found");
            
            return player;
        }
    }
}
