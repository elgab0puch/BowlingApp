﻿namespace Bowling.Core.Application.Implementations.Services
{
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Application.Abstractions.Services;
    using Bowling.Core.Domain.Model;
    using System;
    using System.Threading.Tasks;

    public class FrameService : IFrameService
    {
        private readonly IFrameRepository _frameRepository;
        private readonly IGameRepository _gameRepository;
        
        public FrameService(IFrameRepository frameRepository, IGameRepository gameRepository)
        {
            _frameRepository = frameRepository;
            _gameRepository = gameRepository;
        }

        public async Task<Frame> CreateFrameAsync(Frame frame, Guid gameId)
        {
            var game = await _gameRepository.GetGameByIdAsync(gameId);

            if (game is null)
                throw new ArgumentException($"game with id {gameId} not found");

            if (game.Frames.Count == 10)
                throw new ArgumentException($"game with id {gameId} cannot exceed mor than 10 Frames");

            var newScore = game.Score + frame.TotalKnockedPins;
            game.Score = newScore;
            await _gameRepository.UpdateGameAsync(game);

            frame.Game = game;

            return await _frameRepository.CreateFrameAsync(frame);
        }

        public async Task<Frame> GetFrameByIdAsync(Guid id)
        {
            var frame = await _frameRepository.GetFrameByIdAsync(id);

            if (frame is null)
                throw new ArgumentException($"Frame with id {id} not found");

            return frame;
        }
    }
}
