﻿namespace Bowling.Core.Application.Implementations.Services
{
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Application.Abstractions.Services;
    using Bowling.Core.Domain.Model;
    public class GameService : IGameService
    {
        private readonly IGameRepository _gameRepository;
        private readonly IPlayerRepository _playerRepository;
        public GameService(IGameRepository gameRepository, IPlayerRepository playerRepository)
        {
            _gameRepository = gameRepository;
            _playerRepository = playerRepository;
        }

        public async Task<Game> CreateGameAsync(Game game, Guid playerId)
        {
            var player = await _playerRepository.GetPlayerByIdAsync(playerId);

            if (player is null)
                throw new ArgumentException($"player with id {playerId} not found");

            game.Player = player;

            return await _gameRepository.CreateGameAsync(game);
        }

        public async Task<Game> GetGameByIdAsync(Guid id)
        {
            var game  = await _gameRepository.GetGameByIdAsync(id);

            if (game is null)
                throw new ArgumentException($"Game with id {id} not found");

            return game;
        }
    }
}
