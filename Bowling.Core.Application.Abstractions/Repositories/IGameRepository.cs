﻿namespace Bowling.Core.Application.Abstractions.Repositories
{
    using Bowling.Core.Domain.Model;
    public interface IGameRepository
    {
        public Task<Game> GetGameByIdAsync(Guid id);
        public Task<Game> CreateGameAsync(Game game);
        public Task<Game> UpdateGameAsync(Game game);
    }
}
