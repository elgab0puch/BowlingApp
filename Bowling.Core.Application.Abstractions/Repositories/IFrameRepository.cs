﻿namespace Bowling.Core.Application.Abstractions.Repositories
{
    using Bowling.Core.Domain.Model;
    public interface IFrameRepository
    {
        public Task<Frame> GetFrameByIdAsync(Guid id);
        public Task<Frame> CreateFrameAsync(Frame frame);
    }
}
