﻿namespace Bowling.Core.Application.Abstractions.Repositories
{
    using Bowling.Core.Domain.Model;
    public interface IPlayerRepository
    {
        public Task<Player> GetPlayerByIdAsync(Guid id);
        public Task<Player> GetPlayerByEmailAsync(string email);
        public Task<IEnumerable<Player>> GetAllPlayersAsync();
        public Task<Player> CreatePlayerAsync(Player player);
    }
}
