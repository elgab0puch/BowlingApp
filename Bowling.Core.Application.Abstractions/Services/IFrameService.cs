﻿namespace Bowling.Core.Application.Abstractions.Services
{
    using Bowling.Core.Domain.Model;
    public interface IFrameService
    {
        public Task<Frame> GetFrameByIdAsync(Guid id);
        public Task<Frame> CreateFrameAsync(Frame frame, Guid gameId);
    }
}
