﻿namespace Bowling.Core.Application.Abstractions.Services
{
    using Bowling.Core.Domain.Model;
    public interface IPlayerService
    {
        public Task<Player> GetPlayerByIdAsync(Guid id);

        public Task<IEnumerable<Player>> GetAllPlayersAsync();

        public Task<Player> CreatePlayerAsync(Player player);
    }
}
