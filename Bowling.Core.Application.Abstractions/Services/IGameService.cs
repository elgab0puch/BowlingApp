﻿namespace Bowling.Core.Application.Abstractions.Services
{
    using Bowling.Core.Domain.Model;
    public interface IGameService
    {
        public Task<Game> GetGameByIdAsync(Guid id);
        public Task<Game> CreateGameAsync(Game game, Guid playerId);
    }
}
