﻿namespace Bowling.Infrastructure.Presentation.RestApi.DataTransferObjects
{
    public class GameToCreate
    {
        public Guid PlayerId { get; set; }
        public DateTime Date { get; set; }
    }
}
