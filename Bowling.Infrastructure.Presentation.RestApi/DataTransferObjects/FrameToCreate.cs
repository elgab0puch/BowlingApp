﻿namespace Bowling.Infrastructure.Presentation.RestApi.DataTransferObjects
{
    public class FrameToCreate
    {
        public int TotalKnockedPins { get; set; }
        public bool IsSplit { get; set; }
        public bool IsStrike { get; set; }
        public Guid GameId { get; set; }
    }
}
