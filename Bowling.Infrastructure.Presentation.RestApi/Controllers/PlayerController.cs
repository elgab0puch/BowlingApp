﻿namespace Bowling.Infrastructure.Presentation.RestApi.Controllers
{
    using Bowling.Core.Application.Abstractions.Services;
    using Bowling.Core.Domain.Model;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("/api/players")]
    public class PlayerController : ControllerBase
    {
        public readonly IPlayerService _playerService;

        public PlayerController(IPlayerService playerService)
        {
            _playerService = playerService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPlayers()
        {
            return Ok(await _playerService.GetAllPlayersAsync());
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetPlayerById(Guid id)
        {
            return Ok(await _playerService.GetPlayerByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateUserAsync([FromBody] Player player)
        {
            return new ObjectResult(await _playerService.CreatePlayerAsync(player)) { StatusCode = 201 };
        }
    }
}
