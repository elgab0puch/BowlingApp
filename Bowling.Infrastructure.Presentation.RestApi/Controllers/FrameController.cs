﻿namespace Bowling.Infrastructure.Presentation.RestApi.Controllers
{
    using AutoMapper;
    using Bowling.Core.Application.Abstractions.Services;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Presentation.RestApi.DataTransferObjects;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("/api/frames")]
    public class FrameController : ControllerBase
    {
        public readonly IFrameService _frameService;
        private readonly IMapper _mapper;

        public FrameController(IFrameService frameService, IMapper mapper)
        {
            _frameService = frameService;
            _mapper = mapper;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetFrameById(Guid id)
        {
            return Ok(await _frameService.GetFrameByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateUserAsync([FromBody] FrameToCreate frameToCreate)
        {
            var game = _mapper.Map<Frame>(frameToCreate);
            return new ObjectResult(await _frameService.CreateFrameAsync(game, frameToCreate.GameId)) { StatusCode = 201 };
        }
    }
}
