﻿namespace Bowling.Infrastructure.Presentation.RestApi.Controllers
{
    using AutoMapper;
    using Bowling.Core.Application.Abstractions.Services;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Presentation.RestApi.DataTransferObjects;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("/api/games")]
    public class GameController : ControllerBase
    {
        public readonly IGameService _gameService;
        private readonly IMapper _mapper;

        public GameController(IGameService gameService, IMapper mapper)
        {
            _gameService = gameService;
            _mapper = mapper;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetGameById(Guid id)
        {
            return Ok(await _gameService.GetGameByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateUserAsync([FromBody] GameToCreate gameToCreate)
        {
            var game = _mapper.Map<Game>(gameToCreate);
            return new ObjectResult(await _gameService.CreateGameAsync(game, gameToCreate.PlayerId)) { StatusCode = 201 };
        }
    }
}
