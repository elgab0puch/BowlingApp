﻿namespace Bowling.Infrastructure.Presentation.RestApi.Mappers
{
    using AutoMapper;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Presentation.RestApi.DataTransferObjects;
    public class FrameProfile : Profile
    {
        public FrameProfile()
        {
            CreateMap<FrameToCreate, Frame>();
        }
    }
}
