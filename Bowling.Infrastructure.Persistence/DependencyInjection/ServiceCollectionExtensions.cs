﻿namespace Bowling.Infrastructure.Persistence.DependencyInjection
{
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Infrastructure.Persistence.Implementations.PostgreDb;
    using Microsoft.Extensions.DependencyInjection;
    using System.Reflection;

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDatabaseRepositories(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddScoped<IPlayerRepository, PostgreDbPlayerRepository>();
            services.AddScoped<IGameRepository, PostgreDbGameRepository>();
            services.AddScoped<IFrameRepository, PostgreDbFrameRepository>();
            return services;
        }
    }
}
