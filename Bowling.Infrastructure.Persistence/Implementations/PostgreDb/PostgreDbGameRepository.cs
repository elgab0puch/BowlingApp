﻿namespace Bowling.Infrastructure.Persistence.Implementations.PostgreDb
{
    using AutoMapper;
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    using Microsoft.EntityFrameworkCore;
    public class PostgreDbGameRepository : IGameRepository
    {

        private readonly PostgreDbContext _dbContext;
        private readonly IMapper _mapper;

        public PostgreDbGameRepository(PostgreDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Game> CreateGameAsync(Game game)
        {
            var gameToCreate = _mapper.Map<PostgreDbGame>(game);
            _dbContext.Games.Add(gameToCreate);
            _dbContext.Entry(gameToCreate.Player).State = EntityState.Unchanged;
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<Game>(gameToCreate);
        }

        public async Task<Game> GetGameByIdAsync(Guid id)
        {
            var game = await _dbContext.Games
               .Include(g => g.Frames)
               .Include(g => g.Player)
               .FirstOrDefaultAsync(g => g.Id == id);

            return _mapper.Map<Game>(game);
        }

        public async Task<Game> UpdateGameAsync(Game game)
        {
            var gameToUpdate = _mapper.Map<PostgreDbGame>(game);
            _dbContext.Games.Update(gameToUpdate);
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<Game>(gameToUpdate);
        }
    }
}
