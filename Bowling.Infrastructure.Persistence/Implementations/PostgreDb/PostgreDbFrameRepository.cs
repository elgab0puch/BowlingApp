﻿namespace Bowling.Infrastructure.Persistence.Implementations.PostgreDb
{
    using AutoMapper;
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    using Microsoft.EntityFrameworkCore;

    public class PostgreDbFrameRepository : IFrameRepository
    {
        private readonly PostgreDbContext _dbContext;
        private readonly IMapper _mapper;

        public PostgreDbFrameRepository(PostgreDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Frame> CreateFrameAsync(Frame frame)
        {
            var frameToCreate = _mapper.Map<PostgreDbFrame>(frame);
            _dbContext.Frames.Add(frameToCreate);
            _dbContext.Entry(frameToCreate.Game).State = EntityState.Unchanged;
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<Frame>(frameToCreate);
        }

        public async Task<Frame> GetFrameByIdAsync(Guid id)
        {
            var frame = await _dbContext.Frames
                .Include(f => f.Game)
                .FirstOrDefaultAsync(f => f.Id == id);

            return _mapper.Map<Frame>(frame);
        }
    }
}
