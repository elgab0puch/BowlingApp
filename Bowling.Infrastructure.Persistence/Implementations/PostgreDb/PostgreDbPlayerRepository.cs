﻿namespace Bowling.Infrastructure.Persistence.Implementations.PostgreDb
{
    using AutoMapper;
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;

    internal sealed class PostgreDbPlayerRepository : IPlayerRepository
    {
        private readonly PostgreDbContext _dbContext;
        private readonly IMapper _mapper;

        public PostgreDbPlayerRepository(PostgreDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Player> CreatePlayerAsync(Player player)
        {
            var playerToCreate = _mapper.Map<PostgreDbPlayer>(player);
            _dbContext.Players.Add(playerToCreate);
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<Player>(playerToCreate);
        }

        public async Task<IEnumerable<Player>> GetAllPlayersAsync()
        {
            IEnumerable<PostgreDbPlayer> players = await _dbContext.Players
                .Include(p => p.Games).ToListAsync();

            return _mapper.Map<IEnumerable<Player>>(players);
        }

        public async Task<Player> GetPlayerByEmailAsync(string email)
        {
            var player = await _dbContext.Players
                .FirstOrDefaultAsync(p => p.Email.Equals(email));

            return _mapper.Map<Player>(player);
        }

        public async Task<Player> GetPlayerByIdAsync(Guid id)
        {
            var player = await _dbContext.Players
               .Include(p => p.Games)
               .FirstOrDefaultAsync(p => p.Id.Equals(id));

            return _mapper.Map<Player>(player);
        }
    }
}
