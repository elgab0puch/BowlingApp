﻿namespace Bowling.Infrastructure.Persistence.Implementations
{
    using Microsoft.Extensions.Options;
    using MongoDB.Driver;
    public class MongoDbContext
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _database;

        public MongoDbContext(IOptions<MongoDbSettings> dbOptions)
        {
            var settings = dbOptions.Value;
            _client = new MongoClient(settings.ConnectionString);
            _database = _client.GetDatabase(settings.DatabaseName);
        }

        public IMongoDatabase Database => _database;
        public IMongoClient Client => _client;
    }
}
