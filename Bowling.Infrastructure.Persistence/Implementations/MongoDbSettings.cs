﻿namespace Bowling.Infrastructure.Persistence.Implementations
{
    public class MongoDbSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
