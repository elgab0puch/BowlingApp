﻿namespace Bowling.Infrastructure.Persistence.Implementations.MongoDb
{
    using AutoMapper;
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.MongoDb;
    using MongoDB.Driver;

    public class MongoDbGameRepository : IGameRepository
    {
        private readonly MongoDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IMongoCollection<MongoDbGame> _gamesCollection;

        public MongoDbGameRepository(MongoDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _gamesCollection = _dbContext.Database.GetCollection<MongoDbGame>("Games");
        }

        public async Task<Game> CreateGameAsync(Game game)
        {
            var gameToCreate = _mapper.Map<MongoDbGame>(game);
            await _gamesCollection.InsertOneAsync(gameToCreate);

            return _mapper.Map<Game>(gameToCreate);
        }

        public async Task<Game> GetGameByIdAsync(Guid id)
        {
            var game = await _gamesCollection
             .Find(p => p.Id.Equals(id))
             .FirstOrDefaultAsync();

            return _mapper.Map<Game>(game);
        }

        public async Task<Game> UpdateGameAsync(Game game)
        {
            var gameToUpdate = _mapper.Map<MongoDbGame>(game);
            var updateDefinition = Builders<MongoDbGame>.Update
                .Set(game => game.Score, gameToUpdate.Score);

            await _gamesCollection
                .FindOneAndUpdateAsync(game => game.Id == gameToUpdate.Id, updateDefinition);

            return game;
        }
    }
}
