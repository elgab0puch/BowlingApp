﻿namespace Bowling.Infrastructure.Persistence.Implementations.MongoDb
{
    using AutoMapper;
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.MongoDb;
    using MongoDB.Driver;

    public class MongoDbFrameRepository : IFrameRepository
    {
        private readonly MongoDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IMongoCollection<MongoDbFrame> _framesCollection;

        public MongoDbFrameRepository(MongoDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _framesCollection = _dbContext.Database.GetCollection<MongoDbFrame>("Frames");
        }

        public async Task<Frame> CreateFrameAsync(Frame frame)
        {
            var frameToCreate = _mapper.Map<MongoDbFrame>(frame);
            await _framesCollection.InsertOneAsync(frameToCreate);

            return _mapper.Map<Frame>(frameToCreate);
        }

        public async Task<Frame> GetFrameByIdAsync(Guid id)
        {
            var frame = await _framesCollection
             .Find(f => f.Id.Equals(id))
             .FirstOrDefaultAsync();

            return _mapper.Map<Frame>(frame);
        }
    }
}
