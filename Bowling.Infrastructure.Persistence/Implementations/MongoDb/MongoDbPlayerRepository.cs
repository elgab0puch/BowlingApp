﻿namespace Bowling.Infrastructure.Persistence.Implementations.MongoDb
{
    using AutoMapper;
    using Bowling.Core.Application.Abstractions.Repositories;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.MongoDb;
    using MongoDB.Driver;
    using System;
    using System.Collections.Generic;

    internal sealed class MongoDbPlayerRepository : IPlayerRepository
    {
        private readonly MongoDbContext _dbContext;
        private readonly IMongoCollection<MongoDbPlayer> _playersCollection;
        private readonly IMapper _mapper;

        public MongoDbPlayerRepository(MongoDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _playersCollection = _dbContext.Database.GetCollection<MongoDbPlayer>("Players");
        }

        public async Task<Player> CreatePlayerAsync(Player player)
        {
            var playerToCreate = _mapper.Map<MongoDbPlayer>(player);
            await _playersCollection.InsertOneAsync(playerToCreate);

            return _mapper.Map<Player>(playerToCreate);
        }

        public async Task<IEnumerable<Player>> GetAllPlayersAsync()
        {
            IEnumerable<MongoDbPlayer> players = await _playersCollection
                .Find(_ => true).ToListAsync(); ;

            return _mapper.Map<IEnumerable<Player>>(players);
        }

        public async Task<Player> GetPlayerByEmailAsync(string email)
        {
            var player = await _playersCollection
               .Find(p => p.Email.Equals(email))
               .FirstOrDefaultAsync();

            return _mapper.Map<Player>(player);
        }

        public async Task<Player> GetPlayerByIdAsync(Guid id)
        {
            var player = await _playersCollection
              .Find(p => p.Id.Equals(id))
              .FirstOrDefaultAsync();

            return _mapper.Map<Player>(player);
        }
    }
}
