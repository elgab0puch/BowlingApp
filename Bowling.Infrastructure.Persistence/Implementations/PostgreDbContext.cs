﻿namespace Bowling.Infrastructure.Persistence.Implementations
{
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    using Microsoft.EntityFrameworkCore;
    public sealed class PostgreDbContext : DbContext
    {
        public PostgreDbContext(DbContextOptions<PostgreDbContext> options)
            : base(options)
        {
        }

        public DbSet<PostgreDbPlayer> Players { get; set; }

        public DbSet<PostgreDbGame> Games { get; set; }

        public DbSet<PostgreDbFrame> Frames { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) =>
           modelBuilder.ApplyConfigurationsFromAssembly(typeof(PostgreDbContext).Assembly);
    }
}
