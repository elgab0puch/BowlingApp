﻿namespace Bowling.Infrastructure.Persistence.Mappers.PostgreDb
{
    using AutoMapper;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    public class PostgreDbGameProfile : Profile
    {
        public PostgreDbGameProfile()
        {
            CreateMap<Game, PostgreDbGame>();
            CreateMap<PostgreDbGame, Game>();
        }
    }
}
