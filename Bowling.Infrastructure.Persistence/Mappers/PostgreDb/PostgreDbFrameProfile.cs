﻿namespace Bowling.Infrastructure.Persistence.Mappers.PostgreDb
{
    using AutoMapper;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    public class PostgreDbFrameProfile : Profile
    {
        public PostgreDbFrameProfile()
        {
            CreateMap<Frame, PostgreDbFrame>();
            CreateMap<PostgreDbFrame, Frame>();
        }
    }
}
