﻿namespace Bowling.Infrastructure.Persistence.Mappers.PostgreDb
{
    using AutoMapper;
    using Bowling.Core.Domain.Model;
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;

    public class PostgreDbPlayerProfile : Profile
    {
        public PostgreDbPlayerProfile()
        {
            CreateMap<Player, PostgreDbPlayer>();
            CreateMap<PostgreDbPlayer, Player>();
        }
    }
}
