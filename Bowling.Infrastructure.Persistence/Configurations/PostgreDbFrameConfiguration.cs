﻿namespace Bowling.Infrastructure.Persistence.Configurations
{
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System;
    public class PostgreDbFrameConfiguration : IEntityTypeConfiguration<PostgreDbFrame>
    {
        public void Configure(EntityTypeBuilder<PostgreDbFrame> builder)
        {
            builder.ToTable("frame");
            builder.HasKey(frame => frame.Id);
            builder.Property(frame => frame.Id).ValueGeneratedOnAdd();
            builder.Property(frame => frame.TotalKnockedPins).IsRequired();
            builder.HasOne(f => f.Game)
                .WithMany(g => g.Frames);
        }
    }
}
