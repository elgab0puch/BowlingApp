﻿namespace Bowling.Infrastructure.Persistence.Configurations
{
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System;
    public class PostgreDbGameConfiguration : IEntityTypeConfiguration<PostgreDbGame>
    {
        public void Configure(EntityTypeBuilder<PostgreDbGame> builder)
        {
            builder.ToTable("game");
            builder.HasKey(game => game.Id);
            builder.Property(game => game.Id).ValueGeneratedOnAdd();
            builder.HasOne(g => g.Player)
                .WithMany(p => p.Games);
        }
    }
}
