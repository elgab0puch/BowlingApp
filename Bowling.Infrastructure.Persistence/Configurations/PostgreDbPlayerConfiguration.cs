﻿namespace Bowling.Infrastructure.Persistence.Configurations
{
    using Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    public class PostgreDbPlayerConfiguration : IEntityTypeConfiguration<PostgreDbPlayer>
    {
        public void Configure(EntityTypeBuilder<PostgreDbPlayer> builder)
        {
            builder.ToTable("player");
            builder.HasKey(player => player.Id);
            builder.Property(player => player.Id).ValueGeneratedOnAdd();
            builder.Property(player => player.Email).IsRequired();
            builder.Property(player => player.Name).IsRequired();
        }
    }
}
