﻿namespace Bowling.Infrastructure.Persistence.EntityObjects.MongoDb
{
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    public class MongoDbFrame
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int TotalKnockedPins { get; set; }
        public bool IsSplit { get; set; }
        public bool IsStrike { get; set; }
        public MongoDbGame Game { get; set; }
    }
}
