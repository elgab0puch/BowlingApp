﻿namespace Bowling.Infrastructure.Persistence.EntityObjects.MongoDb
{
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    public class MongoDbGame
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        public DateTime Date { get; set; }
        public int Score { get; set; }
        public MongoDbPlayer Player { get; set; }
        public List<MongoDbFrame> Frames { get; set; }
    }
}
