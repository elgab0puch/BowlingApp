﻿namespace Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb
{
    public class PostgreDbFrame
    {
        public Guid Id { get; set; }
        public int TotalKnockedPins { get; set; }
        public bool IsSplit { get; set; }
        public bool IsStrike { get; set; }
        public PostgreDbGame Game { get; set; }
    }
}
