﻿namespace Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb
{
    public class PostgreDbGame
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public int Score { get; set; }
        public PostgreDbPlayer Player { get; set; }
        public List<PostgreDbFrame> Frames { get; set; }
    }
}
