﻿namespace Bowling.Infrastructure.Persistence.EntityObjects.PostgreDb
{
    public class PostgreDbPlayer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<PostgreDbGame> Games { get; set; }
    }
}
